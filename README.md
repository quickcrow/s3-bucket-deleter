# s3-bucket-deleter

_CAUTION:_ **This program will cause loss to data, use carefully and at your own risk.**

Golang program which will clean a given *versioned* AWS S3 bucket of all objects and object versions, and then delete the bucket.

It will use your default AWS profile if no profile environment variables are exported.

## gotchas
Currently if your AWS access token times out, the program will exit requiring you to rerun.

## running
`go run main.go -bucket $BUCKET_NAME -region $BUCKET_REGION`