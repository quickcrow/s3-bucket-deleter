package main

import (
	"flag"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"os"
	"sync"
)

var bucket string
var region string

func init() {
	flag.StringVar(&bucket, "bucket", "", "The name of the S3 bucket")
	flag.StringVar(&region, "region", "", "The AWS region the S3 bucket is in")
	flag.Parse()

	if bucket == "" || region == "" {
		flag.Usage()
		os.Exit(1)
	}
}

func main() {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: &region,
	}))
	client := s3.New(sess)

	wg := sync.WaitGroup{}
	wg.Add(2)
	ch := make(chan *s3.ListObjectVersionsOutput)
	go getObjectVersions(client, ch, &wg)
	go deleteObjectVersions(client, ch, &wg)
	wg.Wait()

	log.Printf("no more object versions left to delete in %s", bucket)
	deleteS3Bucket(client)
}

func deleteObjectVersions(client *s3.S3, ch chan *s3.ListObjectVersionsOutput, wg *sync.WaitGroup) {
	for listed := range ch {
		var objs []*s3.ObjectIdentifier
		for _, v := range listed.Versions {
			objs = append(objs, &s3.ObjectIdentifier{
				Key:       v.Key,
				VersionId: v.VersionId,
			})
		}
		res, err := client.DeleteObjects(&s3.DeleteObjectsInput{
			Bucket: &bucket,
			Delete: &s3.Delete{
				Objects: objs,
			},
		})
		if err != nil {
			log.Fatalf("error deleting object versions: %v", err)
		}
		log.Println(res)
	}
	wg.Done()
}

func getObjectVersions(client *s3.S3, ch chan *s3.ListObjectVersionsOutput, wg *sync.WaitGroup) {
	for {
		res, err := client.ListObjectVersions(&s3.ListObjectVersionsInput{
			Bucket: &bucket,
		})

		if err != nil {
			log.Fatalf("error listing s3 buckets: %s", err)
		}

		if len(res.Versions) == 0 {
			break
		}
		ch <- res
	}

	close(ch)
	wg.Done()
}

func deleteS3Bucket(client *s3.S3) {
	_, err := client.DeleteBucket(&s3.DeleteBucketInput{
		Bucket: &bucket,
	})
	if err != nil {
		log.Fatalf("error deleting s3 bucket: %v", err)
	}
	log.Printf("s3 bucket: %s deleted", bucket)
}
